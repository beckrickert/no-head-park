var cronjob = require('cron').CronJob;
var fse = require('fs-extra');
var path = require('path');
var crypto = require('crypto');

var fileToWrite = 'hddnopark.tmp';
var writeAntiHeadParkingFile = (fileName, size) => {
    return new Promise((resolve, reject) => {
        console.log(`Checking that ${fileToWrite} exists, will create if not.`);
        fse.ensureFile(path.join(__dirname, './tmp/', fileName))
        .then(() => {
            //write file
            var ws = fse.createWriteStream(path.join(__dirname, './tmp/', fileName));
            console.log(`writing ${fileToWrite}`);
            ws.write(crypto.randomBytes(size));
            ws.end(() => {
                resolve(true);
            });

        })
        .catch((err) => {
            reject(err);
        });
    });
}

var everyFiveSecondsJobs = new cronjob('*/5 * * * * *', () => {

    writeAntiHeadParkingFile(fileToWrite, 1024 * 1024 * 2)
    .catch((err) => {
        everyFiveSecondsJobs.stop();
        console.log("stopping Cron Job due to error");
        console.log(err);
    });

}, null, true, 'America/Vancouver');

everyFiveSecondsJobs.start();