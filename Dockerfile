FROM node:10-slim

# Create app directory
WORKDIR /usr/src/app/

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# If you are building your code for production
# RUN npm ci --only=production
RUN groupmod -g 1000 node && \
    usermod -u 1000 -g 1000 node && \
    npm ci --only=production
# Copy app sourcecode current root folder to current root destination
COPY . .

# runtime to run app
USER node

CMD ["node", "app.js"]
# eg. node app.js

# docker run cmd
# docker build -t rickert-no-head-park /mnt/DOCKER_ROOT/Builds/rickert-no-head-park/
# docker run -d --name rickert-no-head-park --restart=always -v /mnt/DOCKER_ROOT/Volumes/rickert-no-head-park/tmp/:/usr/src/app/tmp/ rickert-no-head-park