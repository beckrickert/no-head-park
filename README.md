# no-head-park

A NodeJS application intended to be used in Docker to prevent head parking of drives that do not work with wdidle3 (Newer WD drives) or other drives which you can't prevent head parking.

## How It Works

Writes a dummy `.tmp` file to `./tmp/` every 5 seconds (WD drives park heads after idling 8 seconds for Blue drives) with a cron job. Ensures that directory exists and creates if it doesn't exist. Creates a dummy file as well if it doesn't exist. 

Intended usage is with a mounted directory that uses the drives you do not want to be parking.

## Usage

```
docker run -d --name your-name-here --restart=always -v your-mount-point/tmp/:/usr/src/app/tmp/ your-name-here
```

## API
A function exists for you to specify file name & extension (default `hddnopark.tmp`) of the dummy file and byte size (default 2048 kb).

## License
[MIT](https://choosealicense.com/licenses/mit/)